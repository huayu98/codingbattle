# Virginia Tech CS4644 Creative Computing Capstone Project - Coding Battle
## G9 Group Members:  
Huayu Liang, Ryan Furuness, Sai Heart Han Hseng, Wenlong Hua

## Online Libraries Used 
* Node.js
* CodeMirror
* PubNub 
* Jdoodle API

## Project Description
Our project is a web-based application called CodingBattle, which enables users to engage in a real-time coding competition 
that helps them improve their programming skills in the Java language.

## How to run the project
* Requirments: Make sures you have downloaded the Node.js installer and installed Node.js and NPM. (Here is the tutorial 
installing link: https://phoenixnap.com/kb/install-node-js-npm-on-windows)

* Steps to start the project:
1. Make sure you are inside the js directory
2. Run "npm install" command line in terminal
3. Run "node test" or "node test.js" command line in terminal
3. Open a website (e.g Chrome)
4. Type http://localhost:3000/MainPage.html and hit enter
5. You should see the main page - click the "PRESS TO START GAME" button to go to the game battle page
6. Scan through the problem description and start typing your answer inside your code editor box
7. Click the "Run" button to check your answer
8. You can also chat with your opponent in the chat box
9. Click the "Leave Race" button to go back to the main page

* Starting a competition between two users:
1. On another computer, download the project resources and repeat the steps above to open another Main page. This 
will start the game and both users will see the chat box and code editors for them and their opponent updating 
in real-time. 


* Unimplemented function(s) in the GameBattle page
1. Multiple problem sets

## Online resources and code used that are not our own
* The directory named plugin in the root directory (which is used for code editor boxes by applying online resource CodeMirror)
* The directory named node_modules inside the js directory (which is used for executing javascript and server, etc by applying online 
resource Node.js)
* All jpeg pictures used for user interface background for the front-end
* The CSS for the "Race Again" button was derived from the TypeRacer website
* The CSS for the problem description was derived from the LeetCode website
