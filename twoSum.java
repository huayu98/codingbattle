//ALL PASS:
int[] result = new int[2];
    Map<Integer, Integer> map = new HashMap<Integer, Integer>();
    for (int i = 0; i < numbers.length; i++) {
        if (map.containsKey(target - numbers[i])) {
            result[1] = i;
            result[0] = map.get(target - numbers[i]);
            return result;
        }
        map.put(numbers[i], i);
    }
    return result;

//TWO PASSES & ONE FAIL:
if (numbers[0] + numbers[1] == target)
      {
        return new int[]{0, 1};
      }
      return new int[]{0,0};

//ONE PASs & TWO FAILS:
return new int[]{1,2};

//ALL FAIL:
return new int[]{0,0};
