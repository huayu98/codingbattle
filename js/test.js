var request = require('request');
const fetch = require('cross-fetch');
const express = require('express');
const app = express();
const cors = require("cors");
app.use(cors())
const port = 3000;
var bodyParser = require('body-parser');
app.use(bodyParser.json());

// For parsing application/json
//app.use(express.json());

// For parsing application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));
// var program = {
//     script : "public class MyClass{public static void main(String args[]) {System.out.println(8);}}",
//     language: "java",
//     versionIndex: "0",
//     clientId: "8856b3a1db044e771794a4f64ff6dc65",
//     clientSecret:"50532c6ec99f9093645f4f8fa9e692f8834e3f471b80646e63f184c4b15c849"
// };
// request({
//     url: 'https://api.jdoodle.com/v1/execute',
//     method: "POST",
//     json: program
// },
// function (error, response, body) {
//     console.log('error:', error);
//     console.log('statusCode:', response && response.statusCode);
//     console.log('body:', body);
// }).then(data => console.log(data));

app.post('/', function (req, res) {
    //console.log(`Value is ${req.body.val}`);
    var program = {
        script : req.body.val,
        language: "java",
        versionIndex: "0",
        // clientId: "8856b3a1db044e771794a4f64ff6dc65",
        // clientSecret:"50532c6ec99f9093645f4f8fa9e692f8834e3f471b80646e63f184c4b15c849"
        clientId: "695027cc1b7eda72d442efeb44ea6baa",
        clientSecret:"e0fba3fe73b34585f947293c264ebdd3c14cff6dd4053c709ce778c3ce24873"

    };
    request({
        url: 'https://api.jdoodle.com/v1/execute',
        method: "POST",
        json: program
    },
    function (error, response, body) {
        console.log('error:', error);
        console.log('statusCode:', response && response.statusCode);
        res.json(body);
    });
});

// var path = require('path');
app.use(express.static('./css'));
app.use(express.static('./images'));

app.get('/MainPage.html', (req, res) => {
  res.sendFile('./MainPage.html', { root: __dirname });
});

app.get('/GameBattle.html', (req, res) => {
    res.sendFile('./GameBattle.html', { root: __dirname });
  });

app.get('/', function (req, res) {
    var program = {
        script : 'public class MyClass{public static void main(String args[]) {System.out.println(8);}}',
        language: "java",
        versionIndex: "0",
        clientId: "8856b3a1db044e771794a4f64ff6dc65",
        clientSecret:"50532c6ec99f9093645f4f8fa9e692f8834e3f471b80646e63f184c4b15c849"
    };
    request({
        url: 'https://api.jdoodle.com/v1/execute',
        method: "POST",
        json: program
    },
    function (error, response, body) {
        console.log('error:', error);
        console.log('statusCode:', response && response.statusCode);
        res.json(body);
    });
});

app.listen(port, function () {
    console.log(`Node Server is running on port: ${port}!`)
});